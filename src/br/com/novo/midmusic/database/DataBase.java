package br.com.novo.midmusic.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DataBase extends SQLiteOpenHelper {

	private static final int VERSION = 2;
	
	public DataBase(Context context) {
		super(context, "db_midimusic", null, VERSION);
	}
	
	@Override
	public void onCreate(SQLiteDatabase db) {
		String sql;
		sql = "CREATE TABLE IF NOT EXISTS tb_config ("
				+ "_id INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL,"
				+ "dica_teclado INTEGER DEFAULT 1"
				+ ");";
		db.execSQL(sql);
		
		sql = "CREATE TABLE IF NOT EXISTS tb_niveis ("
				+ "_id INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL,"
				+ "nivel_nome TEXT"
				+ ");";
		db.execSQL(sql);
		
		sql = "CREATE TABLE IF NOT EXISTS tb_musicas ("
				+ "_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
				+ "musica_nome TEXT,"
				+ "musica_autor TEXT,"
				+ "musica_nivel INTEGER NOT NULL,"
				+ "musica_desbloqueada INTEGER,"
				+ "FOREIGN KEY(musica_nivel) REFERENCES tb_niveis(nivel_id) "
				+ ");";
		db.execSQL(sql);
		
		sql = "INSERT INTO tb_config(dica_teclado) VALUES (1);";
		db.execSQL(sql);
		
		sql = "INSERT INTO tb_niveis(nivel_nome) VALUES (\"1\");";
		db.execSQL(sql);
		sql = "INSERT INTO tb_niveis(nivel_nome) VALUES (\"2\");";
		db.execSQL(sql);
		sql = "INSERT INTO tb_niveis(nivel_nome) VALUES (\"3\");";
		db.execSQL(sql);
		sql = "INSERT INTO tb_niveis(nivel_nome) VALUES (\"4\");";
		db.execSQL(sql);
		sql = "INSERT INTO tb_niveis(nivel_nome) VALUES (\"5\");";
		db.execSQL(sql);
		
		//M�sicas do 1
		sql = "INSERT INTO tb_musicas(musica_nome, musica_autor, musica_nivel, musica_desbloqueada) VALUES (\"Fear Of The Dark\", \"Iron Maiden\", 1, 0);";
		db.execSQL(sql);
		sql = "INSERT INTO tb_musicas(musica_nome, musica_autor, musica_nivel, musica_desbloqueada) VALUES (\"We Will Rock You\", \"Queen\", 1, 0);";
		db.execSQL(sql);
		sql = "INSERT INTO tb_musicas(musica_nome, musica_autor, musica_nivel, musica_desbloqueada) VALUES (\"Rock You Like A Hurricane\", \"Scorpions\", 1, 0);";
		db.execSQL(sql);
		sql = "INSERT INTO tb_musicas(musica_nome, musica_autor, musica_nivel, musica_desbloqueada) VALUES (\"Rock And Roll All Nite\", \"Kiss\", 1, 0);";
		db.execSQL(sql);
		sql = "INSERT INTO tb_musicas(musica_nome, musica_autor, musica_nivel, musica_desbloqueada) VALUES (\"Iron Man\", \"Black Sabbath\", 1, 0);";
		db.execSQL(sql);
		
		//M�sicas do 2
		sql = "INSERT INTO tb_musicas(musica_nome, musica_autor, musica_nivel, musica_desbloqueada) VALUES (\"It\'s My Life\", \"Bon Jovi\", 2, 0);";
		db.execSQL(sql);
		sql = "INSERT INTO tb_musicas(musica_nome, musica_autor, musica_nivel, musica_desbloqueada) VALUES (\"Sweet Child O\'Mine\", \"Guns N Roses\", 2, 0);";
		db.execSQL(sql);
		sql = "INSERT INTO tb_musicas(musica_nome, musica_autor, musica_nivel, musica_desbloqueada) VALUES (\"Back In Black\", \"AC/DC\", 2, 0);";
		db.execSQL(sql);
		sql = "INSERT INTO tb_musicas(musica_nome, musica_autor, musica_nivel, musica_desbloqueada) VALUES (\"Californication\", \"Red Hot Chilli Peppers\", 2, 0);";
		db.execSQL(sql);
		sql = "INSERT INTO tb_musicas(musica_nome, musica_autor, musica_nivel, musica_desbloqueada) VALUES (\"Enter Sandman\", \"Metallica\", 2, 0);";
		db.execSQL(sql);
		
		//M�sicas do 3
		sql = "INSERT INTO tb_musicas(musica_nome, musica_autor, musica_nivel, musica_desbloqueada) VALUES (\"The Kids Aren\'t Alright\", \"The Offspring\", 3, 0);";
		db.execSQL(sql);
		sql = "INSERT INTO tb_musicas(musica_nome, musica_autor, musica_nivel, musica_desbloqueada) VALUES (\"Sweet Dreams\", \"Marilyn Manson\", 3, 0);";
		db.execSQL(sql);
		sql = "INSERT INTO tb_musicas(musica_nome, musica_autor, musica_nivel, musica_desbloqueada) VALUES (\"Eye Of The Tiger\", \"Survivor\", 3, 0);";
		db.execSQL(sql);
		sql = "INSERT INTO tb_musicas(musica_nome, musica_autor, musica_nivel, musica_desbloqueada) VALUES (\"Hotel California\", \"Eagles\", 3, 0);";
		db.execSQL(sql);
		sql = "INSERT INTO tb_musicas(musica_nome, musica_autor, musica_nivel, musica_desbloqueada) VALUES (\"The Final Countdown\", \"Europe\", 3, 0);";
		db.execSQL(sql);
		
		//M�sicas do 4
		sql = "INSERT INTO tb_musicas(musica_nome, musica_autor, musica_nivel, musica_desbloqueada) VALUES (\"Numb\", \"Linkin Park\", 4, 0);";
		db.execSQL(sql);
		sql = "INSERT INTO tb_musicas(musica_nome, musica_autor, musica_nivel, musica_desbloqueada) VALUES (\"Smoke On The Water\", \"Deep Purple\", 4, 0);";
		db.execSQL(sql);
		sql = "INSERT INTO tb_musicas(musica_nome, musica_autor, musica_nivel, musica_desbloqueada) VALUES (\"Paradise\", \"Coldplay\", 4, 0);";
		db.execSQL(sql);
		sql = "INSERT INTO tb_musicas(musica_nome, musica_autor, musica_nivel, musica_desbloqueada) VALUES (\"American Idiot\", \"Green Day\", 4, 0);";
		db.execSQL(sql);
		sql = "INSERT INTO tb_musicas(musica_nome, musica_autor, musica_nivel, musica_desbloqueada) VALUES (\"Smells Like Teen Spirit\", \"Nirvana\", 4, 0);";
		db.execSQL(sql);
		
		//M�sicas do 5
		sql = "INSERT INTO tb_musicas(musica_nome, musica_autor, musica_nivel, musica_desbloqueada) VALUES (\"Carry On Wayward Son\", \"Kansas\", 5, 0);";
		db.execSQL(sql);
		sql = "INSERT INTO tb_musicas(musica_nome, musica_autor, musica_nivel, musica_desbloqueada) VALUES (\"Before I Forget\", \"Slipknot\", 5, 0);";
		db.execSQL(sql);
		sql = "INSERT INTO tb_musicas(musica_nome, musica_autor, musica_nivel, musica_desbloqueada) VALUES (\"Mr Crowley\", \"Ozzy Osbourne\", 5, 0);";
		db.execSQL(sql);
		sql = "INSERT INTO tb_musicas(musica_nome, musica_autor, musica_nivel, musica_desbloqueada) VALUES (\"I Remember You\", \"Skid Row\", 5, 0);";
		db.execSQL(sql);
		sql = "INSERT INTO tb_musicas(musica_nome, musica_autor, musica_nivel, musica_desbloqueada) VALUES (\"Through The Fire And Flames\", \"DragonForce\", 5, 0);";
		db.execSQL(sql);
		
	}
	
	public Boolean isDicaTeclado() {
		Cursor cursor;
		SQLiteDatabase db = getReadableDatabase();
		Boolean dicaTeclado;
		
		cursor = db.query("tb_config", null, null, null, null, null, "_id");
		cursor.moveToFirst();
		if(cursor.getInt(cursor.getColumnIndex("dica_teclado")) == 1){
			dicaTeclado = true;
		}
		else {
			dicaTeclado = false;
		}
		
		return dicaTeclado;
	}
	
	public void desabilitaDicaTeclado() {
		String[] args = {"1"}; //Primeiro registro. Apenas 1 registro.
		SQLiteDatabase db = getWritableDatabase();
		ContentValues cv = new ContentValues();
		cv.put("dica_teclado", "0");
		
		db.update("tb_config", cv, "_id = ?", args);
	}
	
	public Cursor listar(String tabela) {
		Cursor cursor;
		SQLiteDatabase db = getReadableDatabase();
		
		cursor = db.query(tabela, null, null, null, null, null, "_id");
		
		return cursor;
	}
	
	public Cursor listarMusicas(String nivel) {
		Cursor cursor;
		String[] args = {nivel};
		SQLiteDatabase db = getReadableDatabase();
		
		cursor = db.query("tb_musicas", null, "musica_nivel = ?", args, null, null, "_id");
		
		return cursor;
	}
	
	public int getQtdeMusicasNivel(String nivel) {
		Cursor cursor;
		int qtde = 0;
		String[] args = {nivel};
		SQLiteDatabase db = getReadableDatabase();
		
		cursor = db.query("tb_musicas", null, "musica_nivel = ?", args, null, null, "_id");
		
		qtde = cursor.getCount();
		
		return qtde;
	}
	
	public int getNumeroMusicaNivel(String nivel, int musica_id) {
		Cursor cursor;
		int numero_musica_atual = 0;
		String[] args = {nivel};
		SQLiteDatabase db = getReadableDatabase();
		
		cursor = db.query("tb_musicas", null, "musica_nivel = ?", args, null, null, "_id");
		while(cursor.moveToNext()) {
			numero_musica_atual++;
			if(musica_id <= cursor.getInt(cursor.getColumnIndex("_id"))) {
				break;
			}
		}
		
		return numero_musica_atual;
	}
	
	public Cursor getMusica(String musica_id) {
		Cursor cursor;
		String[] args = {musica_id};
		SQLiteDatabase db = getReadableDatabase();
		
		cursor = db.query("tb_musicas", null, "_id = ?", args, null, null, "_id");
		return cursor;
	}
	
	public Cursor getNivel(String nivel_id) {
		Cursor cursor;
		String[] args = {nivel_id};
		SQLiteDatabase db = getReadableDatabase();
		
		cursor = db.query("tb_niveis", null, "_id = ?", args, null, null, "_id");
		return cursor;
	}
	
	public void desbloqueiaMusica(String musica_id) {
		String[] args = {musica_id};
		SQLiteDatabase db = getWritableDatabase();
		ContentValues cv = new ContentValues();
		cv.put("musica_desbloqueada", "1");
		
		db.update("tb_musicas", cv, "_id = ?", args);
	}
	
	public boolean isZerado() {
		Cursor cursor = listar("tb_musicas");
		
		while(cursor.moveToNext()) {
			if(cursor.getInt(cursor.getColumnIndex("musica_desbloqueada")) == 0) {
				// se existir alguma bloqueada, j� retorna falso
				return false;
			}
		}
		
		//Se chegar no final e n�o tiver nenhuma bloqueada, retorna verdadeiro
		return true;
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		
	}

}
