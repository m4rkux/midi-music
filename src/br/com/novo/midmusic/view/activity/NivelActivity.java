package br.com.novo.midmusic.view.activity;

import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import br.com.novo.midmusic.R;
import br.com.novo.midmusic.database.DataBase;
import br.com.novo.midmusic.view.adapter.ItemNivel;

public class NivelActivity extends Activity {

	private ArrayList<String> musicas =  new ArrayList<String>();
	private ArrayList<Integer> musica_ids = new ArrayList<Integer>();
	private String nivel;
	private ListView lista;
	private DataBase database;
	private Cursor cursor;
	private Intent it;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nivel);
        
        carregaPagina();        
        
    }

    @Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    	musicas =  new ArrayList<String>();
    	musica_ids = new ArrayList<Integer>();
    	
    	carregaPagina();
    		
    	super.onActivityResult(requestCode, resultCode, data);
	}

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    
    @Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch(item.getItemId()) {
		case R.id.sobre:
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setTitle(getString(R.string.sobre));
			builder.setMessage(getString(R.string.desenvolvedor) +"\n"+ getString(R.string.email));
			builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
		         public void onClick(DialogInterface dialog, int which) {

		          }
		      });
			
			AlertDialog dialog = builder.create();
			dialog.show();
			break;
		}
		
		return super.onOptionsItemSelected(item);
	}
    
    
    public void carregaPagina() {
    	it = getIntent();
        database = new DataBase(getApplicationContext());
		nivel = String.valueOf(it.getIntExtra("nivel", 0));
		cursor = database.listarMusicas(nivel);
		
		TextView tv_titulo = (TextView) this.findViewById(R.id.tv_titulo);
		tv_titulo.setText(getString(R.string.nivel) + " " + nivel);
		setTitle(getString(R.string.nivel) + " " + nivel);
		
		String musica_nome;
		Integer musica_desbloqueada;
		Integer cont = 0;
		
		while(cursor.moveToNext()) {
			musica_nome = cursor.getString(cursor.getColumnIndex("musica_nome"));
			musica_desbloqueada = cursor.getInt(cursor.getColumnIndex("musica_desbloqueada"));
			musica_ids.add(cursor.getInt(cursor.getColumnIndex("_id")));
			cont++;
			
			if(musica_desbloqueada > 0) {
				musicas.add(musica_nome);
			}
			else {
				musicas.add(" -- "+ getString(R.string.musica) +" "+cont+" --");
			}
		}
        
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getBaseContext(), android.R.layout.simple_list_item_1, musicas);
        
        lista = (ListView) this.findViewById(R.id.lv_musicas1);
        lista.setAdapter(adapter);

        lista.setOnItemClickListener(new OnItemClickListener() {
			@Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent it = new Intent(getApplicationContext(), MusicaActivity.class);
                it.putExtra("musica", musica_ids.get(position));
                startActivityForResult(it, 1);
			}
		});
    }
}
