package br.com.novo.midmusic.view.activity;

import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import br.com.novo.midmusic.R;
import br.com.novo.midmusic.database.DataBase;
import br.com.novo.midmusic.view.adapter.ItemNivel;
import br.com.novo.midmusic.view.adapter.NivelAdapter;

public class NiveisActivity extends Activity {

	private ArrayList<ItemNivel> niveis = new ArrayList<ItemNivel>();
	private DataBase database;
	private ItemNivel itemNivel;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_niveis);

		carregaPagina();		
		
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		niveis = new ArrayList<ItemNivel>();
		
		carregaPagina();
		
		super.onActivityResult(requestCode, resultCode, data);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch(item.getItemId()) {
		case R.id.sobre:
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setTitle(getString(R.string.sobre));
			builder.setMessage(getString(R.string.desenvolvedor) +"\n"+ getString(R.string.email));
			builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
		         public void onClick(DialogInterface dialog, int which) {

		          }
		      });
			
			AlertDialog dialog = builder.create();
			dialog.show();
			break;
		}
		
		return super.onOptionsItemSelected(item);
	}
	
	public void carregaPagina() {
		Cursor c_niveis;
		Cursor c_musicas;
		String texto = new String();
		Boolean nivel_anterior;
		Boolean nivel_atual;
		database = new DataBase(getApplicationContext());
		
		c_niveis = database.listar("tb_niveis");
		nivel_anterior = null;
		nivel_atual = true;
		while(c_niveis.moveToNext()) {
			c_musicas = database.listarMusicas(String.valueOf(c_niveis.getInt(c_niveis.getColumnIndex("_id"))));
			texto = getString(R.string.nivel) + " " +c_niveis.getString(c_niveis.getColumnIndex("nivel_nome"));
			
			Integer total = 0, desbloq = 0;
			Float porc;
			while(c_musicas.moveToNext()) {
				if(c_musicas.getInt(c_musicas.getColumnIndex("musica_desbloqueada")) > 0) {
					desbloq++;
				}
				total++;
			}
			porc = (float) ((desbloq*100)/total);
			if(porc >= 80) {
				nivel_atual = true;
			}
			else {
				nivel_atual = false;
			}
			if(nivel_anterior == null || nivel_anterior == true) {
				itemNivel = new ItemNivel(texto, true, porc);
			}
			else {
				itemNivel = new ItemNivel(texto, false, porc);
			}
			
			nivel_anterior = nivel_atual;
			niveis.add(itemNivel);
		}
		
		
		ListView listView = (ListView) findViewById(R.id.listView1);
	    NivelAdapter myAdapter = new NivelAdapter(this, niveis);
	    listView.setAdapter(myAdapter);  
		
	    listView.setOnItemClickListener(new OnItemClickListener() {
			@Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				
				if(niveis.get(position).getNivel_desbloqueado() == true) {
				
					Intent it = new Intent(getApplicationContext(), NivelActivity.class);
                	position++;
                	it.putExtra("nivel", position);
                	startActivityForResult(it, 1);
				}
            }
		});
	}

}
