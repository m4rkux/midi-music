package br.com.novo.midmusic.view.activity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;
import br.com.novo.midmusic.R;
import br.com.novo.midmusic.database.DataBase;
import br.com.novo.midmusic.view.adapter.Tecla;
import br.com.novo.midmusic.view.adapter.TecladoAdapter;
import br.com.novo.midmusic.view.adapter.TecladoRespostaAdapter;

public class MusicaActivity extends Activity {

	private MediaPlayer player;
	private String musica_id;
	private String musica_nome;
	private String musica_autor;
	private int musica_nivel_id;
	private Integer musica_desbloqueada;
	private DataBase database;
	private GridView teclado;
	private GridView teclado_resposta;
	private EditText et_resposta;
	private TecladoAdapter tecladoAdapter;
	private TecladoRespostaAdapter tecladoRespostaAdapter;
	private Intent it;
	
	private ToggleButton tb_play;
	private TextView tx_musica;
	private ImageView img_musica;
	
	private final int TAMANHO_TECLADO = 24;
	private final int QTDE_LETRAS_ALFABETO = 26;
	private final int BLOQUEADA = 0;
	private final int DESBLOQUEADA = 1;
	private char alfabeto[];
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_musica);
		
		alfabeto = new char[QTDE_LETRAS_ALFABETO];  
		
		char ch;
		int i = 0;
		for( ch = 'A' ; ch <= 'Z' ; ch++ ) {
			alfabeto[i++] = ch;
		}
		
		/* Binds */
		tb_play = (ToggleButton) this.findViewById(R.id.tb_play);
		tx_musica = (TextView) this.findViewById(R.id.tx_musica);
		img_musica = (ImageView) this.findViewById(R.id.img_musica);
		
		et_resposta = (EditText) findViewById(R.id.et_output);
		teclado = (GridView) findViewById(R.id.gv_teclado);
		teclado_resposta = (GridView) findViewById(R.id.gv_teclado_resposta);
		
		TextView tx_pacote = (TextView) findViewById(R.id.tx_pacote);
		TextView tx_progresso_pacote = (TextView) findViewById(R.id.tx_progresso_pacote);
		/* fim - binds*/
		
		it = getIntent();
        final Cursor db_musica, db_nivel;
        String nivel_atual;
		database = new DataBase(getApplicationContext());
		musica_id = String.valueOf(it.getIntExtra("musica", 0));
		db_musica = database.getMusica(musica_id);
		db_musica.moveToFirst();
		
		db_nivel = database.getNivel(db_musica.getString(db_musica.getColumnIndex("musica_nivel")));
		db_nivel.moveToFirst();
		
		nivel_atual = db_nivel.getString(db_nivel.getColumnIndex("nivel_nome"));
		musica_nivel_id = db_nivel.getInt(db_nivel.getColumnIndex("_id"));
		int totalMusicasNivel = database.getQtdeMusicasNivel(nivel_atual);
		int numeroMusicaNivel = database.getNumeroMusicaNivel(nivel_atual, Integer.parseInt(musica_id));
		
		tx_pacote.setText(getString(R.string.nivel) + ": " + nivel_atual);
		tx_progresso_pacote.setText(String.valueOf(numeroMusicaNivel)+ " / "+String.valueOf(totalMusicasNivel));
		
		musica_nome = db_musica.getString(db_musica.getColumnIndex("musica_nome"));
		musica_autor = db_musica.getString(db_musica.getColumnIndex("musica_autor"));
		musica_desbloqueada = db_musica.getInt(db_musica.getColumnIndex("musica_desbloqueada"));
		
		tx_musica.setText(R.string.msg_qual_e_a_musica);
		et_resposta.setText(musica_nome.replaceAll("[A-Za-z0-9]", "_"));
		
		img_musica.setVisibility(View.GONE);
		
		/* Setando a imagem */
		String musica_imagem = musica_nome.toLowerCase().replace(" ", "_").replace("'", "") 
				+ "_" + musica_autor.toLowerCase().replace("/", "").replace(" ", "_").replace("'", "");
		Integer imageId = getResources().getIdentifier(musica_imagem, "drawable", getApplicationContext().getPackageName());
		img_musica.setImageResource(imageId);
		
		/* Preparando player */
		String musica_arquivo = musica_nome.toLowerCase().replace(" ", "_").replace("'", "") 
				+ "_" + musica_autor.toLowerCase().replace("/", "").replace(" ", "_").replace("'", "");
		Integer soundId = getResources().getIdentifier(musica_arquivo, "raw", getApplicationContext().getPackageName());
		
		player = MediaPlayer.create(this, soundId);
		try {
			player.prepare();
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		/* fim - preparando player*/
		
		criaTeclado(musica_nome.toUpperCase().replace(" ", "").replace("'", ""));
		
		/* Verifica se j� foi desbloqueada */
		if(musica_desbloqueada == DESBLOQUEADA) {
			criaTecladoResposta(musica_nome.toUpperCase());
			desbloqueiaMusica();
		}
		else {
			criaTecladoResposta(musica_nome.replaceAll("[A-Za-z0-9]", "_"));
		}
		/* fim - Verifica se j� foi desbloqueada*/
		
		
		
		/* Fun��o de tocar e pausar */
		tb_play.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(tb_play.isChecked()) {
					player.start();
				}
				else {
					player.pause();
				}
				
			}
		});

	}
	
	
	public void verificaResposta() {
		if(!et_resposta.getText().toString().matches(".*[a-zA-Z0-9]+.*")) {
			Toast.makeText(getApplicationContext(), getString(R.string.resposta_em_branco),  Toast.LENGTH_SHORT).show();
		}
		//Se respsota certa
		else if(et_resposta.getText().toString().toLowerCase().trim().equals(musica_nome.toLowerCase())) {
			Toast.makeText(getApplicationContext(), getString(R.string.resposta_certa),  Toast.LENGTH_SHORT).show();
			desbloqueiaMusica();
			
			database.desbloqueiaMusica(musica_id);
			
			if(database.isZerado()) {
				mostraImagemZeramento();
			}
		}
		else {
			Toast.makeText(getApplicationContext(), getString(R.string.resposta_errada),  Toast.LENGTH_LONG).show();
		}
	}
	
	private void criaTeclado(final String palavra) {
		
		final List<Tecla> teclas = new ArrayList<Tecla>();
		
		for (int i = 0; i < TAMANHO_TECLADO; i++) {
		    
		    final Tecla tecla = new Tecla();
		    
		    if(i < palavra.length()) {
		    	tecla.setLetra(palavra.charAt(i));
		    }
		    else {
		    	tecla.setLetra(alfabeto[(int) (Math.random()*26)]);
		    }
		    if(musica_desbloqueada == DESBLOQUEADA) {
		    	tecla.setHabilitado(false);
		    }
		    teclas.add(tecla);
		}
		
		Collections.shuffle(teclas);
		
		tecladoAdapter = new TecladoAdapter(this, teclas);
		
		teclado.setAdapter(tecladoAdapter);
		teclado.setOnItemClickListener(new OnItemClickListener() {
		    @Override
		    public void onItemClick(final AdapterView<?> parent, final View view, final int pos, final long id) {
		    	
		    	if(et_resposta.getText().toString().contains("_")) {
					final Tecla tecla = tecladoAdapter.getItem(pos);
					
					final String aux = et_resposta.getText().toString();
					et_resposta.setText(aux.replaceFirst("_", String.valueOf(tecla.getLetra())));
		
					tecladoRespostaAdapter.alteraPrimeiroUnderline(tecla.getLetra());
					
					// Desabilita a tecla
					tecla.setHabilitado(false);
					
					// Renderiza o teclado novamente
					tecladoAdapter.notifyDataSetChanged();
					
					if(database.isDicaTeclado()) {
						mostraMensagemDeAjudaDoTeclado();
					}
		    	}
		    	
		    	if(!et_resposta.getText().toString().contains("_")) {
		    		verificaResposta();
		    	}
		    }
		});
	}
	
	private void desbloqueiaMusica() {
		img_musica.setVisibility(View.VISIBLE);
		tx_musica.setText(musica_autor+" - "+musica_nome);
		setTitle(getString(R.string.musica) + ": " +musica_nome);
		
		tecladoAdapter.disableAllItems();
		teclado.setVisibility(View.GONE);
		
		tecladoRespostaAdapter.disableAllItems();
	}
		
	private void criaTecladoResposta(final String palavra) {
		
		final List<Tecla> teclas = new ArrayList<Tecla>();
		
		for (int i = 0; i < palavra.length(); i++) {
		    
		    final Tecla tecla = new Tecla();
		    
		    tecla.setLetra(palavra.charAt(i));
		    tecla.setHabilitado(false);
		    teclas.add(tecla);
		}
		
		tecladoRespostaAdapter = new TecladoRespostaAdapter(this, teclas);
		
		teclado_resposta.setAdapter(tecladoRespostaAdapter);
		teclado_resposta.setOnItemClickListener(new OnItemClickListener() {
		    @Override
		    public void onItemClick(final AdapterView<?> parent, final View view, final int pos, final long id) {
				final Tecla tecla = tecladoRespostaAdapter.getItem(pos);
	
				tecladoAdapter.restauraTecla(tecla.getLetra());
				StringBuilder resposta = new StringBuilder(et_resposta.getText().toString());
				resposta.setCharAt(tecla.getPosicao(), '_');
				et_resposta.setText(resposta);
				tecladoRespostaAdapter.alteraLetraAtual(tecla);
				// Desabilita a tecla
				tecla.setHabilitado(false);
				
				// Renderiza o teclado novamente
				tecladoRespostaAdapter.notifyDataSetChanged();
		    }
		});
	}
	
	public void paraMusica() {
		 if(player.isPlaying() ) {
			player.stop();
		 }
		 player.release();
	}
	
	@Override
	public void onBackPressed() {
		paraMusica();
		super.onBackPressed();
	}
	
	public void mostraImagemZeramento() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		
		View imgZeramentoView = View.inflate(this, R.layout.layout_zeramento, null);
		
		builder.setTitle(getString(R.string.parabens));
		builder.setView(imgZeramentoView);
		builder.setPositiveButton("OK", null);
		
		AlertDialog dialog = builder.create();
		dialog.show();
	}
	
	public void mostraMensagemDeAjudaDoTeclado() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		final CharSequence[] items = {" N�o exibir novamente "};
		
		View checkBoxView = View.inflate(this, R.layout.checkbox, null);
		final CheckBox checkBox = (CheckBox) checkBoxView.findViewById(R.id.checkbox);
		
		builder.setTitle(getString(R.string.dica));
		builder.setMessage(getString(R.string.voce_pode_corrigir_a_resposta));
		builder.setView(checkBoxView);
		
		builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
	         public void onClick(DialogInterface dialog, int which) {
	        	 if(checkBox.isChecked()) {
	        		 database.desabilitaDicaTeclado();
	        	 }
	          }
	      });
		
		AlertDialog dialog = builder.create();
		dialog.show();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch(item.getItemId()) {
			case R.id.sobre:
				AlertDialog.Builder builder = new AlertDialog.Builder(this);
				builder.setTitle(getString(R.string.sobre));
				builder.setMessage(getString(R.string.desenvolvedor) +"\n"+ getString(R.string.email));
				builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			         public void onClick(DialogInterface dialog, int which) {
	
			          }
			      });
				
				AlertDialog dialog = builder.create();
				dialog.show();
				break;
			  case android.R.id.home:
				  paraMusica();
				  break;
		}
		
		return super.onOptionsItemSelected(item);
	}

}
