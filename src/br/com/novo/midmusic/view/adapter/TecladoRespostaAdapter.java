package br.com.novo.midmusic.view.adapter;

import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.TextView;
import br.com.novo.midmusic.R;

@SuppressLint("ViewHolder")
public class TecladoRespostaAdapter extends BaseAdapter implements ListAdapter {

    private final List<Tecla> teclas;
    private final Context context;

    public TecladoRespostaAdapter(final Context context, final List<Tecla> items) {
		this.context = context;
		this.teclas = items;
    }

    @Override
    public int getCount() {
    	return teclas.size();
    }

    @Override
    public Tecla getItem(final int position) {
    	return teclas.get(position);
    }

    @Override
    public long getItemId(final int position) {
    	return position;
    }

    @Override
    public View getView(final int position, final View convertView, final ViewGroup parent) {
		final Tecla tecla = getItem(position);
		final LayoutInflater inflater = LayoutInflater.from(context);
		final View view = inflater.inflate(R.layout.tecla_resposta_layout, null);
		
		tecla.setPosicao(position);
		final TextView botao = (TextView) view.findViewById(R.id.bt_tecla);
		botao.setText(String.valueOf(tecla.getLetra()));
		botao.setEnabled(tecla.isHabilitado());
	
		return view;
    }
    
    @Override
    public boolean areAllItemsEnabled() {
    	for(Tecla tecla : teclas){
    		if(!tecla.isHabilitado()){
    			return false;
    		}
    	}
        return true;
    }
    
    public void disableAllItems(){
    	for(Tecla tecla : teclas){
    		tecla.setHabilitado(false);
    	}
    	notifyDataSetChanged();
    }
    
    public void alteraPrimeiroUnderline(char letra) {
    	for(Tecla tecla : teclas){
    		if(tecla.getLetra() == '_') {
    			tecla.setLetra(letra);
    			tecla.setHabilitado(true);
    			break;
    		}
    	}
    	notifyDataSetChanged();
    }
    
    @Override
    public boolean isEnabled(int position) {
        return getItem(position).isHabilitado();
    }

	public void alteraLetraAtual(Tecla tecla) {
		tecla.setLetra('_');
		tecla.setHabilitado(false);
	}
	
	public int getPosicao(Tecla tecla) {
		return tecla.getPosicao();
	}

	
}
