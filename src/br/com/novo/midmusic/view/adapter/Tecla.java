package br.com.novo.midmusic.view.adapter;

public class Tecla {

	private int posicao;
    private char letra;
    private boolean habilitado;
    
    public Tecla() {
    	this.habilitado = true;
    }

    public char getLetra() {
    	return letra;
    }

    public void setLetra(final char letra) {
    	this.letra = letra;
    }

    public boolean isHabilitado() {
    	return habilitado;
    }

    public void setHabilitado(final boolean habilitado) {
    	this.habilitado = habilitado;
    }

    @Override
    public boolean equals(final Object outro) {
    	return this.letra == ((Tecla) outro).letra;
    }

	public int getPosicao() {
		return posicao;
	}

	public void setPosicao(int posicao) {
		this.posicao = posicao;
	}

}
