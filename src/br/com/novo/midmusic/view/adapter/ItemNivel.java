package br.com.novo.midmusic.view.adapter;

public class ItemNivel {
	private String nivel;
	private Boolean nivel_desbloqueado;
	private Float nivel_porcentagem;
	
	public ItemNivel(String nivel, Boolean nivel_desbloqueado, Float nivel_porcentagem){
		setNivel(nivel);
		setNivel_desbloqueado(nivel_desbloqueado);
		setNivel_porcentagem(nivel_porcentagem);
	}
	
	public Boolean getNivel_desbloqueado() {
		return nivel_desbloqueado;
	}
	public void setNivel_desbloqueado(Boolean nivel_desbloqueado) {
		this.nivel_desbloqueado = nivel_desbloqueado;
	}
	public String getNivel() {
		return nivel;
	}
	public void setNivel(String nivel) {
		this.nivel = nivel;
	}
	public Float getNivel_porcentagem() {
		return nivel_porcentagem;
	}
	public void setNivel_porcentagem(Float nivel_porcentagem) {
		this.nivel_porcentagem = nivel_porcentagem;
	}
	
	
}
