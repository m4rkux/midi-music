package br.com.novo.midmusic.view.adapter;

import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.TextView;
import br.com.novo.midmusic.R;

@SuppressLint("ViewHolder")
public class TecladoAdapter extends BaseAdapter implements ListAdapter {

    private final List<Tecla> teclas;
    private final Context context;

    public TecladoAdapter(final Context context, final List<Tecla> items) {
		this.context = context;
		this.teclas = items;
    }

    @Override
    public int getCount() {
    	return teclas.size();
    }

    @Override
    public Tecla getItem(final int position) {
    	return teclas.get(position);
    }

    @Override
    public long getItemId(final int position) {
    	return position;
    }

    @Override
    public View getView(final int position, final View convertView, final ViewGroup parent) {
		final Tecla tecla = getItem(position);
		final LayoutInflater inflater = LayoutInflater.from(context);
		final View view = inflater.inflate(R.layout.tecla_layout, null);
		
		final TextView botao = (TextView) view.findViewById(R.id.bt_tecla);
		botao.setText(String.valueOf(tecla.getLetra()));
		botao.setEnabled(tecla.isHabilitado());
	
		return view;
    }
    
    @Override
    public boolean areAllItemsEnabled() {
    	for(Tecla tecla : teclas){
    		if(!tecla.isHabilitado()){
    			return false;
    		}
    	}
        return true;
    }
    
    public void disableAllItems(){
    	for(Tecla tecla : teclas){
    		tecla.setHabilitado(false);
    	}
    	notifyDataSetChanged();
    }
    
    @Override
    public boolean isEnabled(int position) {
        return getItem(position).isHabilitado();
    }

	public void restauraTecla(char letra) {
		for(Tecla tecla : teclas){
    		if(tecla.getLetra() == letra && !tecla.isHabilitado()) {
    			tecla.setHabilitado(true);
    			notifyDataSetChanged();
    			break;
    		}
    	}
		
	}

}
