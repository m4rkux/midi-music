package br.com.novo.midmusic.view.adapter;

import java.util.List;

import android.content.Context;
import android.support.v4.view.ViewPager.LayoutParams;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import br.com.novo.midmusic.R;

public class NivelAdapter extends BaseAdapter{

	List<ItemNivel> lista;
	Context context;
	
	public NivelAdapter(final Context context, final List<ItemNivel> lista) {
		this.lista = lista;
		this.context = context;
	}
	
	@Override
	public int getCount() {
		return lista.size();
	}

	@Override
	public Object getItem(final int arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(final int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, final ViewGroup arg2) {
		ViewHolder viewHolder;
		if (convertView == null) {
			final LayoutInflater inflater = LayoutInflater.from(context);
			convertView = inflater.inflate(R.layout.item_nivel, null);
			viewHolder = new ViewHolder();
			viewHolder.tv_nivel = (TextView) convertView.findViewById(R.id.tv_nivel);
			viewHolder.tv_porcentagem = (TextView) convertView.findViewById(R.id.tv_porcentagem);
			viewHolder.imageView = (ImageView) convertView.findViewById(R.id.img_cadeado);
			convertView.setTag(viewHolder);
		}
		else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		viewHolder.tv_nivel.setText(lista.get(position).getNivel());
		viewHolder.tv_porcentagem.setText(lista.get(position).getNivel_porcentagem() + "%");
		if(lista.get(position).getNivel_desbloqueado() == true) {
			viewHolder.imageView.setVisibility(View.GONE);
			convertView.setEnabled(true);
		}
		else {
			convertView.setEnabled(false);
		}
		return convertView;
	}
	
	static class ViewHolder {
		LayoutParams layout;
		TextView tv_nivel;
		TextView tv_porcentagem;
		ImageView imageView;
	}

}
